<?php

/**
 * Page callback: Interfax settings
 *
 * @see Interfax_menu()
 */
function interfax_admin_form_settings($form, &$form_state) {
/*
 	$result = db_select('interfax_settings','f') //get the file just uploaded
		->fields('f', array('id','username', 'password', 'faxnumber', 'csid', 'pageheader', 'subject', 'replyemail', 'page_size', 'page_orientation', 'page'))
		->condition('id', 1, '=')
		->execute()
		->fetchAssoc();
*/
	//return $result;
	
  $form = array();
  
  $form['interfax']['interfax_username'] = array(
    '#type' 			=> 'textfield',
    '#title' 			=> t('Interfax Username'),
    '#size' 			=> 60,
    '#maxlength' 		=> 60,
    '#description' 		=> t('Enter your Interfax Username here.'),
    '#required' 		=> TRUE,
    '#default_value'  	=> variable_get('interfax_username', '')
    //'#default_value'  	=> isset($result['username']) ? $result['username'] : '',
  );
  $form['interfax']['interfax_password'] = array(
    '#type' 			=> 'textfield',
    '#title'		 	=> t('Interfax Password'),
    '#size' 			=> 60,
    '#maxlength' 		=> 60,
    '#description' 		=> t('Enter your Interfax Password here.'),
    '#required' 		=> TRUE,
    '#default_value' 	=> variable_get('interfax_password', '')
    //'#default_value' 	=> isset($result['password']) ? $result['password'] : '',
  );
  $form['interfax']['interfax_faxnumber'] = array(
    '#type' 			=> 'textfield',
    '#title' 			=> t('Interfax Fax Number'),
    '#size' 			=> 60,
    '#maxlength' 		=> 60,
    '#description' 		=> t('Enter your Interfax Fax Number here.'),
    '#default_value' 	=> variable_get('interfax_faxnumber', '')
    //'#default_value' 	=> isset($result['faxnumber']) ? $result['faxnumber'] : '',
  );
  $form['interfax']['interfax_csid'] = array(
    '#type' => 'textfield',
    '#title' => t('Interfax csid'),
    '#size' => 60,
    '#maxlength' => 60,
    '#description' => t('Enter your Interfax csid here.'),
    '#required' => TRUE,
    '#default_value' 	=> variable_get('interfax_csid', '')
    //'#default_value' => isset($result['csid']) ? $result['csid'] : '',
  );
  $form['interfax']['interfax_pageheader'] = array(
    '#type' => 'textfield',
    '#title' => t('Interfax Default Page Header'),
    '#size' => 60,
    '#maxlength' => 60,
    '#description' => t('Enter your Default Page Header here.'),
    '#required' => TRUE,
    '#default_value' 	=> variable_get('interfax_pageheader', '')
    //'#default_value' => isset($result['pageheader']) ? $result['pageheader'] : '',
  );  
  $form['interfax']['interfax_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Interfax Default Subject'),
    '#size' => 60,
    '#maxlength' => 60,
    '#description' => t('Enter your Default Subject here.'),
    '#required' => TRUE,
    '#default_value' 	=> variable_get('interfax_subject', '')
    //'#default_value' => isset($result['subject']) ? $result['subject'] : '',
  );
  $form['interfax']['interfax_replyemail'] = array(
    '#type' => 'textfield',
    '#title' => t('Interfax Reply Email'),
    '#size' => 60,
    '#maxlength' => 60,
    '#description' => t('Enter your Interfax Reply Email here.'),
    '#required' => TRUE,
    '#default_value' 	=> variable_get('interfax_replyemail', '')
    //'#default_value' => isset($result['replyemail']) ? $result['replyemail'] : '',
  );
  
  $form['interfax']['interfax_page_size'] = array(
    '#title' => t('Interfax page size'),
    '#type' => 'radios',
    '#options' => array(
    	'Letter' => t('Letter'), 
    	'A4' => t('A4')
    ),
    '#description' => t('Select your Interfax page_size here.'),
    '#required' => TRUE,
    '#default_value' 	=> variable_get('interfax_page_size', '')
    //'#default_value' => isset($result['page_size']) ? $result['page_size'] : '',
  );

  $form['interfax']['interfax_page_orientation'] = array(
    '#title' => t('Interfax page_orientation'),
    '#type' => 'radios',
    //'#default_value' => variable_get('page_orientation', 'Portrait'),
    '#options' => array(
    	'Portrait' => t('Portrait'),
    	'Landscape' => t('Landscape')
    ),
    '#description' => t('Select your Interfax page_orientation here.'),
    '#required' => TRUE,
    '#default_value' 	=> variable_get('interfax_page_orientation', '')
    //'#default_value' => isset($result['page_orientation']) ? $result['page_orientation'] : '',
  );

  $form['interfax']['interfax_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Interfax page url'),
    '#size' => 100,
    '#maxlength' => 100,
    '#description' => t('Enter your sites defaul faxing url here.'),
    '#default_value' 	=> variable_get('interfax_page', '')
    //'#default_value' => isset($result['page']) ? $result['page'] : '',
  );
  //$form['#submit'][] = 'interfax_admin_form_settings_submit';
  
  return system_settings_form($form);
}

